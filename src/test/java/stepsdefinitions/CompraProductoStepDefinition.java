package stepsdefinitions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Utilidades.DataDrivenExcel;
import Utilidades.Excel;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.CompraProductoStep;

public class CompraProductoStepDefinition {

	@Steps
	CompraProductoStep compraProductoStep;

	DataDrivenExcel dataDriverExcel = new DataDrivenExcel();
	Map<String, String> data = new HashMap<String, String>();

	@Given("^el cliente ingresa a la pagina falabella (\\d+)$")
	public void elClienteIngresaALaPaginaFalabella(int row, DataTable dataExcel) throws Exception {
		List<Map<String, String>> dataFeature = dataExcel.asMaps(String.class, String.class);
		Excel excel = new Excel(dataFeature.get(0).get("Ruta Excel"), dataFeature.get(0).get("Pestana"), true, row);
		data = dataDriverExcel.leerExcel(excel);
		compraProductoStep.homeOpen();
	}

	@When("^se busca el producto a comprar$")
	public void seBuscaElProductoAComprar() throws Exception {
		compraProductoStep.paginafalabella(data.get("Pagina"), data.get("BuscarProducto"));
	}

	@Then("^se realiza la compra$")
	public void seRealizaLaCompra() throws Exception {
		compraProductoStep.comprarproducto(data.get("Seguro"));
		compraProductoStep.comparar(data.get("Seguro"));
	}

}
