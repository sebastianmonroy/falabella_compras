package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = {"src/test/resources/feature/CompraProducto.feature"},
		glue = "",
		monochrome= true,
		snippets = SnippetType.CAMELCASE,		
		tags = {"@tag1"}
		)
public class CompraProductoRunner {
}