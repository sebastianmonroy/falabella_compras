package pages;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CompraProductoPage extends PageObject {
	
	
	@FindBy(xpath = "//*[@id=\"testId-pagination-top-arrow-right\"]")
	WebElementFacade btnsiguientepagina;
	
	@FindBy(xpath = "//*[@id='testId-searchResults-products']/div[3]/div/a/div[2]/div[3]/button")
	WebElementFacade btnagregaralabolsa;
	
	@FindBy(xpath = "//*[@id=\"linkButton\"]")
	WebElementFacade btnverbolsacompra;

	@FindBy(xpath = "//*[@id=\"root\"]/div[2]/div[1]/section/form/section/div[1]/div/div[2]/span[1]")
	WebElementFacade lblpreciototal;
	
	@FindBy(xpath = "//*[@id='root']/div[2]/div[1]/section/section/div/div/div/form/div/a")
	WebElementFacade opcionseguroDropdown;
	
	@FindBy(xpath = "//*[@id='root']/div[2]/div[2]/div[1]/form/div[2]/div[2]/div[2]/button")
	WebElementFacade btniracomprar;
	
	@FindBy(xpath = "//*[@id='fbra_checkoutOrderSummary']/div/section/div/div/ol/li/div/div[1]/span[3]")
	WebElementFacade lblpreciototalresumen;

	@FindBy(xpath = "//*[@id='fbra_checkoutOrderSummary']/div/section/div/div/ol/li/div/div[1]/span[4]")
	WebElementFacade lblsegurototalresumen;
	
	@FindBy(xpath = "//*[@id='root']/div[2]/div[1]/section/section/div/div/div/form/div/div/ul/li")
	List<WebElementFacade> seguroList;

	String value = "";

	public void compra(String Seguro) {
		
		btnsiguientepagina.click();

		//espera hasta que el DOM cargue
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//se mueve hasta el elemento
		withAction().moveToElement(btnagregaralabolsa).perform();

		btnagregaralabolsa.click();
		btnverbolsacompra.click();
		String lblPretotal = (lblpreciototal.getText().replace(" ",""));
		value = lblPretotal;
		System.out.println(lblPretotal);

		waitFor(ExpectedConditions.visibilityOf(opcionseguroDropdown));
		opcionseguroDropdown.click();
		
		//realiza un for de la lista de seguros y trae el seguro seteado en la data
		for(int i = 0; i < seguroList.size(); i++) {
			if(seguroList.get(i).getText().equalsIgnoreCase(Seguro)) {
				seguroList.get(i).click();
				break;
			}
		}

		//espera hasta que el DOM cargue
		chromeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);

		waitFor(ExpectedConditions.visibilityOf(btniracomprar));
		btniracomprar.click(); 
		
	}
	public void precioProducto(String Seguro) {
		waitFor(ExpectedConditions.visibilityOf(lblpreciototalresumen));

		String lblPre = lblpreciototalresumen.getText();
		System.out.println(lblPre);

		String[] precioSeguro = Seguro.split(" ");
		System.out.println(precioSeguro[3]+precioSeguro[4]);
		String precioSeguroF = (precioSeguro[3]+precioSeguro[4]);

		waitFor(ExpectedConditions.visibilityOf(lblsegurototalresumen));
		String lblSeg = lblsegurototalresumen.getText();
		System.out.println(lblSeg);

		//compara que los precios sean iguales
		assertTrue("Verification failed: los precios no son iguales.",lblPre.equals((value)));
		assertTrue("Verification failed: los precios no son iguales.",lblSeg.equals((precioSeguroF)));
	}
	
}
