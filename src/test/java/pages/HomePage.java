package pages;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.google.com/")

public class HomePage extends PageObject {
	
	@FindBy(xpath = "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")
	WebElementFacade txtbusqueda;
	
	@FindBy(xpath = "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[2]/div[2]/div[2]/center/input[1]")
	WebElementFacade btnbuscar;
	
	@FindBy(xpath = "//*[@id=\"rso\"]/div[1]/div/div/div/div/div/div/div[1]/a/h3")
	WebElementFacade linkfalabella;
	
	@FindBy(xpath = "//*[@id=\"fake-close\"]")
	WebElementFacade popupavisoimportante;
	
	@FindBy(xpath = "//*[@id=\"acc-alert-close\"]")
	WebElementFacade popuploquequieres;
	
	@FindBy(xpath = "//*[@id=\"testId-SearchBar-Input\"]")
	WebElementFacade txtbuscarproducto;
	
	@FindBy(xpath = "//*[@id=\"testId-search-wrapper\"]/div/button")
	WebElementFacade btnbuscarproducto;

	public void ingreso(String Pagina, String BuscarProducto){

		//eleimina cookies del navegador
		getDriver().manage().deleteAllCookies();


		txtbusqueda.sendKeys(Pagina);
		btnbuscar.click();
		linkfalabella.click();
		popupavisoimportante.click();
		popuploquequieres.click();
		txtbuscarproducto.sendKeys(BuscarProducto);
		btnbuscarproducto.click();
	}
}
