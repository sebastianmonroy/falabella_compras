package steps;

import net.thucydides.core.annotations.Step;
import pages.CompraProductoPage;
import pages.HomePage;

public class CompraProductoStep {
	
	HomePage homePage;
	CompraProductoPage compraProductoPage;
	
	@Step
	public void homeOpen() {
		homePage.open();
	}
	
	@Step
	public void paginafalabella(String Pagina, String BuscarProducto) {
		homePage.ingreso(Pagina, BuscarProducto);
	}
	
	@Step
	public void comprarproducto(String Seguro) {
		compraProductoPage.compra(Seguro);
	}
	@Step
	public void comparar(String Seguro) {
		compraProductoPage.precioProducto(Seguro);
	}
}
