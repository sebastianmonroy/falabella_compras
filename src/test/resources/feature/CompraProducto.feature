#Author: Sebastian Monroy
#Fecha:  26-05-2021
Feature: como usuario deseo realizar
	la compra de un producto

  @tag1
  Scenario Outline: compra de producto
    Given el cliente ingresa a la pagina falabella <fila>
    | Ruta Excel                               | Pestana |
    | src/test/resources/data/DatosCompra.xlsx | Hoja1   |
    When se busca el producto a comprar
    Then se realiza la compra
    
    Examples:
     | fila  |
     |   1   |